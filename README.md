# TicketBox

## How to
### Build
```
$ ./gradlew -Dfile.encoding=UTF-8 build
```

### Run
```
$ ./gradlew -Dfile.encoding=UTF-8 bootRun
$ # Open http://localhost:8080/
```

### Test
```
$ ./gradlew -Dfile.encoding=UTF-8 test
```

### ETC
테스트용 username/password는 아래와 같습니다.
```
# user1
username : user
password : 1

# user2
username : other
password : 2
```