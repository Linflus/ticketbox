package us.linfl.ticket.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import us.linfl.ticket.exception.ReservationException;
import us.linfl.ticket.model.Member;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.Reservation;
import us.linfl.ticket.model.Seat;
import us.linfl.ticket.repository.ReservationRepository;
import us.linfl.ticket.repository.SeatRepository;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ReservationServiceTests {
    @Autowired
    ReservationService reservationService;
    @MockBean
    ReservationRepository reservationRepository;
    @MockBean
    MemberService memberService;
    @MockBean
    SeatRepository seatRepository;

    @Mock
    UserDetails userDetails;

    private Play createPlay(int playId) {
        return new Play(playId);
    }

    private Seat createSeat(Play play, Integer seat) {
        return new Seat(play, seat,null);
    }

    private List<Seat> createSeats(Play play, List<Integer> seats) {
        return seats.stream()
                .map(i -> createSeat(play, i))
                .collect(Collectors.toList());
    }

    @Test
    public void shouldBeAvailableIfNotReserved() {
        Seat seat = createSeat(createPlay(1), 1);
        assertThat(seat.isAvailable()).isTrue();
    }

    @Test
    public void shouldNotBeAvailableIfReserved() {
        Seat seat = createSeat(createPlay(1), 1);
        seat.setReservation(new Reservation());
        assertThat(seat.isAvailable()).isFalse();
    }

    @Test
    public void shouldBeTrueAllSeatsAreAvailable() {
        Play play = createPlay(1);
        List<Seat> seats = createSeats(play, Arrays.asList(1, 2, 3));

        //given
        given(seatRepository.findByPlayAndNo(any(Play.class), any(Integer.class)))
                .willAnswer(i -> Optional.of(
                        new Seat(i.getArgument(0), i.getArgument(1), null)
                ));

        //when
        boolean isAvailable = reservationService.isAvailable(play, seats);

        //then
        then(seatRepository)
                .should(times(3))
                .findByPlayAndNo(eq(play), any(Integer.class));

        assertThat(isAvailable).isTrue();
    }

    @Test
    public void shouldBeFalseAnySeatIsNotAvailable() {
        Play play = createPlay(1);
        List<Seat> seats = createSeats(play, Arrays.asList(1, 2, 3));

        //given
        given(seatRepository.findByPlayAndNo(any(Play.class), not(eq(2))))
                .willAnswer(i -> Optional.of(
                        new Seat(i.getArgument(0), i.getArgument(1), null)
                ));
        given(seatRepository.findByPlayAndNo(any(Play.class), eq(2)))
                .willAnswer(i -> Optional.of(
                        new Seat(i.getArgument(0), i.getArgument(1), new Reservation())
                ));

        //when
        boolean isAvailable = reservationService.isAvailable(play, seats);

        //then
        assertThat(isAvailable).isFalse();
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldNotAddReservationWhenSeatsIsNull() {
        Reservation reservation = new Reservation();

        //given

        //when
        reservationService.addReservation(userDetails, reservation);

        //then
        then(reservationRepository)
                .should(never())
                .save(any(Reservation.class));
        then(seatRepository)
                .should(never())
                .saveAll(anyList());
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldNotAddReservationWhenSeatsIsEmpty() {
        Reservation reservation = new Reservation();
        reservation.setSeats(Collections.emptyList());

        //given

        //when
        reservationService.addReservation(userDetails, reservation);

        //then
        then(reservationRepository)
                .should(never())
                .save(any(Reservation.class));
        then(seatRepository)
                .should(never())
                .saveAll(anyList());
    }

    @Test
    public void shouldAddReservationIfSeatsAreAvailable() {
        Reservation reservation = new Reservation(1, Arrays.asList(1,2,3));

        //given
        given(seatRepository.findByPlayAndNo(any(Play.class), any(Integer.class)))
                .willAnswer(i -> Optional.of(
                        new Seat(i.getArgument(0), i.getArgument(1), null)
                ));

        given(userDetails.getUsername())
                .willReturn("user");

        given(memberService.getMemberByUserDetails(userDetails))
                .willReturn(new Member("user"));

        //when
        reservationService.addReservation(userDetails, reservation);

        //then
        then(reservationRepository)
                .should()
                .save(any(Reservation.class));
        then(seatRepository)
                .should()
                .saveAll(anyList());

        assertThat(reservation.getMember()).isNotNull();
        assertThat(reservation.getMember().getUsername()).isEqualTo("user");
    }

    @Test(expected = ReservationException.class)
    public void shouldNotAddReservationIfSeatsAreNotAvailable() {
        Reservation reservation = new Reservation(1, Arrays.asList(1,2,3));

        //given
        given(seatRepository.findByPlayAndNo(any(Play.class), not(eq(2))))
                .willAnswer(i -> Optional.of(
                        new Seat(i.getArgument(0), i.getArgument(1), null)
                ));
        given(seatRepository.findByPlayAndNo(any(Play.class), eq(2)))
                .willAnswer(i -> Optional.of(
                        new Seat(i.getArgument(0), i.getArgument(1), reservation)
                ));

        //when
        reservationService.addReservation(userDetails, reservation);

        //then
        then(reservationRepository)
                .should(never())
                .save(any(Reservation.class));
        then(seatRepository)
                .should(never())
                .saveAll(anyList());
    }

    @Test(expected = ReservationException.class)
    public void shouldNotCancelOtherMembersReservation() {
        Member member = new Member("other");
        Reservation reservation = new Reservation(1, Arrays.asList(3,4));
        reservation.setMember(member);

        //given
        given(reservationRepository.findById(1))
                .willReturn(Optional.of(reservation));

        given(userDetails.getUsername())
                .willReturn("user");

        given(memberService.getMemberByUserDetails(userDetails))
                .willReturn(new Member("user"));
        //when
        reservationService.cancelReservation(userDetails, 1);

        //then
        then(seatRepository)
                .should(never())
                .saveAll(anyIterable());

        then(reservationRepository)
                .should(never())
                .delete(any(Reservation.class));
    }

    @Test
    public void shouldCancelMyReservation() {
        Member member = new Member("user");
        Reservation reservation = new Reservation(1, Arrays.asList(3,4));
        reservation.setMember(member);
        List<Seat> seats = reservation.getSeats();
        //given
        given(reservationRepository.findById(1))
                .willReturn(Optional.of(reservation));

        given(userDetails.getUsername())
                .willReturn("user");

        given(memberService.getMemberByUserDetails(userDetails))
                .willReturn(new Member("user"));
        //when
        reservationService.cancelReservation(userDetails, 1);

        //then
        then(seatRepository)
                .should()
                .saveAll(seats);

        then(reservationRepository)
                .should()
                .delete(reservation);
    }
}
