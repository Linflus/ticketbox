package us.linfl.ticket.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import us.linfl.ticket.model.Member;
import us.linfl.ticket.repository.MemberRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;


@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberServiceTests {
    @Autowired
    MemberService memberService;

    @Mock
    UserDetails userDetails;

    @MockBean
    MemberRepository memberRepository;

    @Test
    public void shouldGetMemberByUserDetails() {
        //given
        given(userDetails.getUsername())
                .willReturn("user");
        given(memberRepository.findByUsername("user"))
                .willReturn(new Member());

        //when
        Member member = memberService.getMemberByUserDetails(userDetails);

        //then
        then(memberRepository)
                .should()
                .findByUsername("user");
        assertThat(member).isNotNull();
    }

    @Test
    public void shouldNotGetMemberIfNotExist() {
        //given
        given(userDetails.getUsername())
                .willReturn("user");
        given(memberRepository.findByUsername("user"))
                .willReturn(null);

        //when
        Member member = memberService.getMemberByUserDetails(userDetails);

        //then
        then(memberRepository)
                .should()
                .findByUsername("user");

        assertThat(member).isNull();
    }

    @Test
    public void shouldAddMemberIfNotExist() {
        //given
        given(userDetails.getUsername())
                .willReturn("user");
        given(memberRepository.findByUsername("user"))
                .willReturn(null);

        //when
        Member member = memberService.getOrAddMemberByUserDetails(userDetails);

        //then
        then(memberRepository)
                .should()
                .findByUsername("user");

        assertThat(member).isNotNull();
    }

    @Test
    public void shouldNotAddMemberIfExist() {
        //given
        given(userDetails.getUsername())
                .willReturn("user");
        given(memberRepository.findByUsername("user"))
                .willReturn(new Member());

        //when
        memberService.getOrAddMemberByUserDetails(userDetails);

        //then
        then(memberRepository)
                .should(never())
                .save(any(Member.class));
    }
}
