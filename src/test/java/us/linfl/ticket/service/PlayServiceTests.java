package us.linfl.ticket.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.Screen;
import us.linfl.ticket.model.Seat;
import us.linfl.ticket.repository.PlayRepository;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PlayServiceTests {
    @Autowired
    PlayService playService;

    @MockBean
    PlayRepository playRepository;
    @Mock
    List<Seat> seats;

    private Play play;

    @Before
    public void setUp() {
        play = new Play();
        Screen screen = new Screen();
        screen.setNumberOfSeats(10);
        play.setScreen(screen);
        play.setSeats(seats);
    }

    @Test
    public void shouldGetPlayById() {
        //given
        given(playRepository.findById(1))
                .willReturn(Optional.of(play));

        //when
        playService.getPlay(1);

        //then
        then(playRepository).should().findById(1);
    }

    @Test
    public void shouldCreateSeatWhenEmpty() {
        //given
        given(seats.isEmpty())
                .willReturn(true);
        given(playRepository.findById(1))
                .willReturn(Optional.of(play));

        //when
        playService.getPlayWithSeats(1);

        //then
        then(playRepository)
                .should().findById(1);
        then(seats)
                .should(times(10)).add(any(Seat.class));
    }

    @Test
    public void shouldNotCreateSeatWhenNotEmpty() {
        //given
        given(seats.isEmpty())
                .willReturn(false);
        given(playRepository.findById(1))
                .willReturn(Optional.of(play));

        //when
        playService.getPlayWithSeats(1);

        //then
        then(playRepository)
                .should().findById(1);
        then(seats)
                .should(never())
                .add(any(Seat.class));
    }
}
