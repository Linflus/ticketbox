package us.linfl.ticket.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import us.linfl.ticket.config.SecurityConfiguration;
import us.linfl.ticket.exception.ReservationException;
import us.linfl.ticket.model.Member;
import us.linfl.ticket.model.Reservation;
import us.linfl.ticket.service.MemberService;
import us.linfl.ticket.service.ReservationService;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(MemberController.class)
@AutoConfigureJsonTesters
@Import(SecurityConfiguration.class)
public class MemberControllerTests {
    @MockBean
    ReservationService reservationService;

    @MockBean
    MemberService memberService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    JacksonTester<Reservation> json;

    @Test
    @WithMockUser(username = "user")
    public void shouldGetMeIfAuthenticated() throws Exception {
        given(this.memberService.getOrAddMemberByUserDetails(any(UserDetails.class)))
                .willReturn(new Member("user"));

        this.mockMvc.perform(get("/api/members/me")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.username").value("user"));
    }

    @Test
    public void shouldNotGetMeIfAuthenticated() throws Exception {
        this.mockMvc.perform(get("/api/members/me")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "user")
    public void shouldAddReservationIfValidUser() throws Exception {
        String serialized = "{\"play\":1,\"seats\":[1,2]}";
        Reservation reservation = json.parseObject(serialized);

        given(reservationService.addReservation(any(UserDetails.class), any(Reservation.class)))
                .willReturn(reservation);

        this.mockMvc.perform(post("/api/members/user/reservations")
                .content(serialized)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = "other")
    public void shouldNotAddReservationIfNotValidUser() throws Exception {
        String serialized = "{\"play\":1,\"seats\":[1,2]}";
        this.mockMvc.perform(post("/api/members/user/reservations")
                .content(serialized)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user")
    public void shouldAddReservationIfNotAvailableSeats() throws Exception {
        String serialized = "{\"play\":1,\"seats\":[1,2]}";

        given(reservationService.addReservation(any(UserDetails.class), any(Reservation.class)))
                .willThrow(ReservationException.class);

        this.mockMvc.perform(post("/api/members/user/reservations")
                .content(serialized)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    @WithMockUser(username = "user")
    public void shouldAddReservationIfNotFoundSeats() throws Exception {
        String serialized = "{\"play\":1,\"seats\":[1,2]}";

        given(reservationService.addReservation(any(UserDetails.class), any(Reservation.class)))
                .willThrow(NoSuchElementException.class);

        this.mockMvc.perform(post("/api/members/user/reservations")
                .content(serialized)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "user")
    public void shouldGetReservationsIfAuthenticated() throws Exception {

        given(reservationService.getReservations(any(UserDetails.class)))
                .willReturn(new ArrayList<>());

        this.mockMvc.perform(get("/api/members/user/reservations")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

    @Test
    @WithMockUser(username = "user")
    public void shouldDeleteReservationsIfValidUser() throws Exception {
        this.mockMvc.perform(delete("/api/members/user/reservations/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(username = "user")
    public void shouldNotDeleteReservationsIfNotFound() throws Exception {
        doThrow(NoSuchElementException.class)
                .when(reservationService).cancelReservation(any(UserDetails.class), eq(1));

        this.mockMvc.perform(delete("/api/members/user/reservations/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "other")
    public void shouldNotDeleteReservationsIfNotValidUser() throws Exception {
        doThrow(ReservationException.class)
                .when(reservationService).cancelReservation(any(UserDetails.class), eq(1));

        this.mockMvc.perform(delete("/api/members/user/reservations/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}
