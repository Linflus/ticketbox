package us.linfl.ticket.exception;

import org.springframework.dao.DataAccessException;

public class ReservationException extends DataAccessException {
    public ReservationException(String msg) {
        super(msg);
    }

    public ReservationException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
