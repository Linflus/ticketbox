package us.linfl.ticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.linfl.ticket.model.TimeTable;
import us.linfl.ticket.service.TimeTableService;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/timetable")
public class TimeTableController {
    @Autowired
    TimeTableService timeTableService;

    @GetMapping
    public ResponseEntity<TimeTable> getTimeTable() {
        TimeTable timeTable = timeTableService.getTimeTable(LocalDate.now());
        return new ResponseEntity<>(timeTable, HttpStatus.OK);
    }
}
