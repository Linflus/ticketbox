package us.linfl.ticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import us.linfl.ticket.exception.ReservationException;
import us.linfl.ticket.model.Member;
import us.linfl.ticket.model.Reservation;
import us.linfl.ticket.service.MemberService;
import us.linfl.ticket.service.ReservationService;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/members")
public class MemberController {
    @Autowired
    ReservationService reservationService;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    MemberService memberService;

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/me")
    public ResponseEntity<Member> getMe(Authentication authentication) {
        Member me = memberService.getOrAddMemberByUserDetails((UserDetails)authentication.getPrincipal());
        return new ResponseEntity<>(me, HttpStatus.OK);
    }

    @PreAuthorize("isAuthenticated() and (#username == authentication.name)")
    @PostMapping(value = "/{username}/reservations")
    public ResponseEntity<Reservation> addReservation(@PathVariable("username") String username, @RequestBody Reservation reservation) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        try {
            reservation = reservationService.addReservation(userDetails, reservation);
            return new ResponseEntity<>(reservation, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (ReservationException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("isAuthenticated() and (#username == authentication.name)")
    @GetMapping("/{username}/reservations")
    public ResponseEntity<List<Reservation>> getReservations(@PathVariable("username") String username) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        List<Reservation> reservations = reservationService.getReservations(userDetails);
        return new ResponseEntity<>(reservations, HttpStatus.OK);
    }

    @PreAuthorize("isAuthenticated() and (#username == authentication.name)")
    @DeleteMapping("/{username}/reservations/{reservationId}")
    public ResponseEntity<Void> deleteReservation(@PathVariable("username") String username, @PathVariable("reservationId") int reservationId) {
        try{
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            reservationService.cancelReservation(userDetails, reservationId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (ReservationException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}
