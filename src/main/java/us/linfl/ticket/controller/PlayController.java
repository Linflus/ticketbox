package us.linfl.ticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.Seat;
import us.linfl.ticket.service.PlayService;

import java.util.List;

@RestController
@RequestMapping("/api/plays")
public class PlayController {
    @Autowired
    PlayService playService;

    @GetMapping("/{playId}")
    public ResponseEntity<Play> getPlay(@PathVariable("playId") int playId) {
        Play play = playService.getPlay(playId);
        return new ResponseEntity<>(play, HttpStatus.OK);
    }

    @GetMapping("/{playId}/seats")
    public ResponseEntity<List<Seat>> getSeats(@PathVariable("playId") int playId) {
        Play play = playService.getPlayWithSeats(playId);
        return new ResponseEntity<>(play.getSeats(), HttpStatus.OK);
    }
}
