package us.linfl.ticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.Seat;
import us.linfl.ticket.repository.PlayRepository;

import java.util.List;

@Service
public class JpaPlayService implements PlayService {
    @Autowired
    PlayRepository playRepository;

    @Transactional(readOnly = true)
    @Override
    public Play getPlay(int playId) {
        return playRepository.findById(playId).get();
    }

    @Transactional
    @Override
    public Play getPlayWithSeats(int playId) throws DataAccessException {
        Play play = this.getPlay(playId);
        List<Seat> seats = play.getSeats();

        if(seats.isEmpty()) {
            for(int i = 0; i < play.getScreen().getNumberOfSeats(); i++) {
                seats.add(new Seat(play, i+1, null));
            }
            playRepository.save(play);
        }

        return play;
    }
}
