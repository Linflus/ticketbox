package us.linfl.ticket.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import us.linfl.ticket.model.TimeTable;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;

public class JsonTimeTableService implements TimeTableService {
    @Override
    public TimeTable getTimeTable(LocalDate date) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("static/data/timetable.json").getFile());
        TimeTable timeTable = null;
        try {
            timeTable = new ObjectMapper().readValue(file, TimeTable.class);
        } catch (IOException e) {
            e.printStackTrace();
            timeTable = new TimeTable();
            timeTable.setDate(date);
            timeTable.setMovies(Collections.emptyList());
        }
        return timeTable;
    }
}
