package us.linfl.ticket.service;


import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import us.linfl.ticket.model.Member;

public interface MemberService {
    Member getMemberByUserDetails(UserDetails userDetails) throws DataAccessException;
    Member getOrAddMemberByUserDetails(UserDetails userDetails) throws DataAccessException;
}
