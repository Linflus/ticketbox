package us.linfl.ticket.service;

import org.springframework.dao.DataAccessException;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.Seat;

import java.util.List;

public interface PlayService {
    Play getPlay(int playId) throws DataAccessException;
    Play getPlayWithSeats(int playId) throws DataAccessException;
}
