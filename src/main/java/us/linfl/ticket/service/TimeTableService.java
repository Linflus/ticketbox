package us.linfl.ticket.service;

import org.springframework.dao.DataAccessException;
import us.linfl.ticket.model.Movie;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.TimeTable;

import java.time.LocalDate;

public interface TimeTableService {
    TimeTable getTimeTable(LocalDate date) throws DataAccessException;
}
