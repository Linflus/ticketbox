package us.linfl.ticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import us.linfl.ticket.model.Member;
import us.linfl.ticket.repository.MemberRepository;

@Service
public class JpaMemberService implements MemberService {
    @Autowired
    MemberRepository memberRepository;

    @Transactional
    @Override
    public Member getOrAddMemberByUserDetails(UserDetails userDetails) {
        Member member = getMemberByUserDetails(userDetails);
        if(member == null) {
            member = new Member(userDetails.getUsername());
            memberRepository.save(member);
        }
        return member;
    }

    @Transactional(readOnly = true)
    @Override
    public Member getMemberByUserDetails(UserDetails userDetails) {
        return memberRepository.findByUsername(userDetails.getUsername());
    }
}
