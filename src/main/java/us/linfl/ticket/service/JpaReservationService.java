package us.linfl.ticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import us.linfl.ticket.exception.ReservationException;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.Reservation;
import us.linfl.ticket.model.Seat;
import us.linfl.ticket.repository.ReservationRepository;
import us.linfl.ticket.repository.SeatRepository;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class JpaReservationService implements ReservationService {
    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    MemberService memberService;
    @Autowired
    SeatRepository seatRepository;

    @Transactional(readOnly = true)
    @Override
    public boolean isAvailable(Play play, List<Seat> seats) {
        for(Seat seat : seats) {
            if(!seatRepository.findByPlayAndNo(play, seat.getNo()).get().isAvailable())
                return false;
        }
        return true;
    }

    @Transactional
    @Override
    public Reservation addReservation(UserDetails userDetails, Reservation reservation) throws DataAccessException {
        List<Seat> seats = reservation.getSeats();
        if(seats == null || seats.isEmpty()) {
            throw new NoSuchElementException("선택된 좌석이 없습니다.");
        }

        reservation.setMember(memberService.getMemberByUserDetails(userDetails));
        if(isAvailable(reservation.getPlay(), seats)) {
            reservation = reservationRepository.save(reservation);
            seatRepository.saveAll(seats);
            return reservation;
        }
        throw new ReservationException("이미 예약된 좌석입니다.");
    }

    @Transactional(readOnly = true)
    @Override
    public List<Reservation> getReservations(UserDetails userDetails) throws DataAccessException {
        return reservationRepository.findByMember(memberService.getMemberByUserDetails(userDetails));
    }

    @Transactional
    @Override
    public void cancelReservation(UserDetails userDetails, int reservationId) throws DataAccessException {
        Reservation reservation = reservationRepository.findById(reservationId).get();
        if(reservation.getMember().getUsername().equals(userDetails.getUsername())) {
            List<Seat> seats = reservation.getSeats();
            seats.forEach(seat -> seat.setReservation(null));
            seatRepository.saveAll(seats);
            reservation.setSeats(Collections.emptyList());
            reservationRepository.delete(reservation);
            return;
        }
        throw new ReservationException("예약을 취소할 수 없습니다.");
    }
}
