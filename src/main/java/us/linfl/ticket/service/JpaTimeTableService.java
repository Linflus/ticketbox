package us.linfl.ticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import us.linfl.ticket.model.Movie;
import us.linfl.ticket.model.TimeTable;
import us.linfl.ticket.repository.MovieRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class JpaTimeTableService implements TimeTableService {
    @Autowired
    MovieRepository movieRepository;

    @Transactional(readOnly = true)
    @Override
    public TimeTable getTimeTable(LocalDate date) {
        TimeTable timeTable = new TimeTable();
        timeTable.setDate(date);
        List<Movie> movies = new ArrayList<>();
        movieRepository.findAll().forEach(movies::add);
        timeTable.setMovies(movies);
        return timeTable;
    }
}
