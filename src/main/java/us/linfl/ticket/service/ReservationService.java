package us.linfl.ticket.service;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.Reservation;
import us.linfl.ticket.model.Seat;

import java.util.List;

public interface ReservationService {
    boolean isAvailable(Play play, List<Seat> seats);
    Reservation addReservation(UserDetails userDetails, Reservation reservation) throws DataAccessException;
    List<Reservation> getReservations(UserDetails userDetails) throws DataAccessException;
    void cancelReservation(UserDetails userDetails, int reservationId) throws DataAccessException;
}

