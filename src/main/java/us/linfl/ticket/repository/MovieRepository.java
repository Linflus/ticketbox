package us.linfl.ticket.repository;

import org.springframework.data.repository.CrudRepository;
import us.linfl.ticket.model.Movie;

public interface MovieRepository extends CrudRepository<Movie, Integer> {
}
