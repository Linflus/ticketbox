package us.linfl.ticket.repository;

import org.springframework.data.repository.CrudRepository;
import us.linfl.ticket.model.Screen;

public interface ScreenRepository extends CrudRepository<Screen, Integer> {
}
