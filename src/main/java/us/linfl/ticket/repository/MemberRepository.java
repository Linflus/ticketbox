package us.linfl.ticket.repository;

import org.springframework.data.repository.CrudRepository;
import us.linfl.ticket.model.Member;

public interface MemberRepository extends CrudRepository<Member, Integer> {
    Member findByUsername(String username);
}
