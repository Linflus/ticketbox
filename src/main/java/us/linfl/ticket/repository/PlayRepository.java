package us.linfl.ticket.repository;

import org.springframework.data.repository.CrudRepository;
import us.linfl.ticket.model.Play;

public interface PlayRepository extends CrudRepository<Play, Integer> {

}
