package us.linfl.ticket.repository;

import org.springframework.data.repository.CrudRepository;
import us.linfl.ticket.model.Play;
import us.linfl.ticket.model.Seat;

import java.util.Optional;

public interface SeatRepository extends CrudRepository<Seat, Seat.SeatId> {
    Optional<Seat> findByPlayAndNo(Play play, Integer no);
}
