package us.linfl.ticket.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "members")
@Getter
@Setter
public class Member {
    @Id
    private String username;

    @JsonIgnore
    @OneToMany(mappedBy = "member", fetch = FetchType.LAZY)
    private List<Reservation> reservations;

    public Member() {}

    public Member(String username) {
        this.username = username;
    }
}
