package us.linfl.ticket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "movies")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie {
    @Id
    private Integer id;

    private String title;

    @OneToMany(mappedBy = "movie")
    @JsonManagedReference
    private List<Play> plays;

    public Movie() {}

    public Movie(String id) {
        this.id = Integer.parseInt(id);
    }

    public void setPlays(List<Play> plays) {
        for(Play play : plays) {
            play.setMovie(this);
        }
        this.plays = plays;
    }
}
