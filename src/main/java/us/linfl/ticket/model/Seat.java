package us.linfl.ticket.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="seats")
@IdClass(Seat.SeatId.class)
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "no")
public class Seat {
    public static class SeatId implements Serializable {
        private Integer play;
        private Integer no;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "play_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Play play;

    @Id
    private Integer no;

    @ManyToOne
    @JoinColumn(name = "reservation_id")
    @JsonIgnore
    private Reservation reservation;

    public boolean isAvailable() {
        return reservation == null;
    }

    public Seat() {}

    public Seat(Play play, Integer seat, Reservation reservation) {
        this.play = play;
        this.no = seat;
        this.reservation = reservation;
    }
}
