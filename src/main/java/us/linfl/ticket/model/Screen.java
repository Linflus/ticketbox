package us.linfl.ticket.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "screens")
@Getter
@Setter
public class Screen {
    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private Integer numberOfSeats;
}
