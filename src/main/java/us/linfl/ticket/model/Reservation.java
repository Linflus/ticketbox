package us.linfl.ticket.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "reservations")
@Getter
@Setter
public class Reservation {
    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "play_id")
    private Play play;

    @OneToMany(mappedBy = "reservation", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JsonIdentityReference(alwaysAsId = true)
    private List<Seat> seats;

    @ManyToOne
    @JoinColumn(name = "member_username")
    private Member member;

    public Reservation() {}

    @JsonCreator
    public Reservation(@JsonProperty("play") Integer play, @JsonProperty("seats") List<Integer> seats) {
        this.play = new Play(play);
        this.seats = new ArrayList<>();
        for(Integer seat : seats) {
            this.seats.add(new Seat(this.play, seat, this));
        }
    }

    public Movie getMovie() {
        return this.play.getMovie();
    }
}
