package us.linfl.ticket.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class TimeTable {
    private LocalDate date;
    private List<Movie> movies;
}
