package us.linfl.ticket.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "plays")
@Getter
@Setter
public class Play {
    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "movie_id")
    @JsonBackReference
    private Movie movie;

    @ManyToOne
    @JoinColumn(name = "screen_id")
    private Screen screen;

    private String start;

    private String end;

    @JsonIgnore
    @OneToMany(mappedBy = "play", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @OrderBy("play, no ASC")
    private List<Seat> seats;

    public Play() {}

    public Play(Integer id) {
        this.id = id;
    }
}
