/* global Handlebars */
(($, Handlebars, console) => {
  $(document).ready(() => {
    new Pages().goTo('time-table');
  });

  class TemplateComponent {
    constructor(selector, templateSelector) {
      this.$element = $(selector);
      this.template = Handlebars.compile($(templateSelector).html());
    }

    clear() {
      this.$element.empty();
    }

    render(data) {
      this.$element.html(this.template(data));
    }
  }

  class Dialog {
    constructor(selector) {
      this.$element = $(selector);
      this.$apply = new Button(this.$element.find('.btn-primary'), this.apply.bind(this));
    }

    open() {

    }
    // eslint-disable-next-line no-unused-vars
    apply(data) {
      return true;
    }
  }

  class Button {
    constructor(selector, onClick) {
      this.$element = $(selector);
      this.$element.on('click', (event) => onClick.bind(this)($(event.currentTarget).data()));
    }
  }

  class Pages extends TemplateComponent {
    constructor() {
      super('#navbarSupportedContent', '#nav-template');

      this.state = {current: 'time-table'};
      this.contents = {
        'time-table': new TimeTable('#container', (playId) => {
          this.reservationDialog = this.reservationDialog  || new ReservationDialog(() => this.goTo('my-page'));
          this.reservationDialog.open(playId);
        }),
        'my-page': new MyPage('#container')
      };
    }

    goTo(page) {
      this.state.current = page;
      this.render(this.state);
      this.$element.find('a.nav-link').on('click', (event) => {
        let page = $(event.currentTarget).data('page');
        this.goTo(page);
      });
      this.contents[page].load();
    }
  }

  class TimeTable extends TemplateComponent {
    constructor(selector, onReserve){
      super(selector, '#time-table-template');
      this.api = new ResourceCollection('/api/timetable');
      this.onReserve = onReserve;
    }

    load() {
      this.clear();
      this.api.list()
        .then((timetable) => {
          this.render(timetable);
          new Button(this.$element.find('.reserve-play'), (data) => {
            let playId = data['play'];
            this.onReserve(playId);
          });
        })
        .catch((err) => {
          console.error(err);
          alert('상영시간표 조회에 실패하였습니다.');
        });
    }
  }

  class Seats extends TemplateComponent {
    constructor(playId){
      super('#select-seats .seats-list', '#seats-template');
      this.api = new ResourceCollection('/api/plays/'+encodeURIComponent(playId)+'/seats');
    }

    load() {
      this.clear();
      this.api.list()
        .then((seats) => {
          this.render({'seats':seats});
        }).catch((err) => {
          console.error(err);
          alert('좌석 조회에 실패하였습니다.');
        });
    }
  }

  class ReservationDialog extends Dialog {
    constructor(onCompleteReservation) {
      super('#select-seats');
      this.onCompleteReservation = onCompleteReservation;
    }

    open(play) {
      this.play = play;
      new Seats(play).load();
    }

    apply() {
      let checkedSeats = $.map(this.$element.find('.seats-list input:checked'), (el) => $(el).data('no'));

      let apiPromise = new ResourceCollection('/api/members').get('me').then((me) => {
        return new ResourceCollection('/api/members/'+encodeURIComponent(me.username)+'/reservations');
      });

      apiPromise.then((api) => api.create({
        play: this.play,
        seats: checkedSeats
      })).then((reservation) => {
        if(reservation) {
          alert('예약이 완료 되었습니다.');
          this.onCompleteReservation(reservation);
        }
      }).catch((err) => {
        let errMessage = '예약이 실패하였습니다.';
        if(err instanceof HttpNotOkError) {
          switch (err.status) {
          case 401:
            errMessage = '로그인이 필요합니다.';
            break;
          case 404:
            errMessage = '좌석을 선택해주세요.';
            break;
          case 409:
            errMessage = '이미 예약된 좌석입니다.';
            break;
          }
        }
        alert(errMessage);
      });
    }
  }

  class MyPage extends TemplateComponent {
    constructor(selector) {
      super(selector, '#my-page-template');
    }

    load() {
      this.clear();

      let apiPromise = new ResourceCollection('/api/members').get('me').then((me) => {
        this.me = me;
        return new ResourceCollection('/api/members/'+encodeURIComponent(me.username)+'/reservations');
      });

      apiPromise.then((api) => {
        api.list().then((reservations) => {
          let isEmpty = !reservations || reservations.length === 0;
          this.render({'reservations': reservations, isEmpty: isEmpty, 'username': this.me.username});
          new Button(this.$element.find('button.cancel-reservation'), (data) => {
            api.remove(data['reservation']).then(() => this.load()).catch((err) => {
              let errMessage = '취소가 실패하였습니다.';
              if (err instanceof HttpNotOkError) {
                switch (err.status) {
                case 403:
                  errMessage = '본인이 예약한 좌석만 취소 가능합니다.';
                  break;
                case 404:
                  errMessage = '취소할 좌석이 없습니다.';
                  break;
                }
              }
              alert(errMessage);
            });
          });
        });
      }).catch((err) => {
        if(err instanceof HttpNotOkError && err.status === 401) {
          alert('로그인이 필요합니다.');
        }
      });
    }
  }

  class ResourceCollection {
    constructor(endpoint) {
      this.endpoint = endpoint;
    }

    fetchJson(url, options) {
      options = options || {
        method: 'GET'
      };
      options.headers = options.headers || new Headers({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      });

      return fetch(url, options).then((res) => {
        if(!res.ok && res.status >= 400)
          throw new HttpNotOkError(res.status);

        return res.json().catch(() => null);
      });
    }

    create(resource) {
      return this.fetchJson(this.endpoint, {
        method:'POST',
        body: JSON.stringify(resource)
      });
    }

    list() {
      return this.fetchJson(this.endpoint);
    }

    get(resourceId) {
      return this.fetchJson(this.endpoint + '/' + encodeURIComponent(resourceId));
    }

    remove(resourceId) {
      return this.fetchJson(this.endpoint + '/' + encodeURIComponent(resourceId), {
        method: 'DELETE'
      });
    }
  }

  class HttpNotOkError extends Error {
    constructor(status, message) {
      super(message);
      this.statusCode = status;
    }
    get status() { return this.statusCode; }
  }

  Handlebars.registerHelper('ifEquals', function(v1, v2, options) {
    if(v1 === v2) {
      return options.fn(this);
    }
    return options.inverse(this);
  });
})(jQuery, Handlebars, window.console);
