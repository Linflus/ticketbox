INSERT INTO screens(id, name, number_of_seats) VALUES(1, '1관 6층', 158);
INSERT INTO screens(id, name, number_of_seats) VALUES(2, '2관 6층', 124);
INSERT INTO screens(id, name, number_of_seats) VALUES(3, '3관 8층', 172);
INSERT INTO screens(id, name, number_of_seats) VALUES(4, '4관 8층', 124);
INSERT INTO screens(id, name, number_of_seats) VALUES(5, '5관 10층', 172);
INSERT INTO screens(id, name, number_of_seats) VALUES(6, '6관 10층', 124);

INSERT INTO movies(id, title) VALUES(80890, '공작');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(1, '15:45', '18:12', 80890, 6);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(2, '26:40', '29:07', 80890, 6);

INSERT INTO movies(id, title) VALUES(80910, '나를 차버린 스파이');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(3, '16:35', '18:42', 80910, 4);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(4, '08:00', '10:07', 80910, 6);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(5, '13:20', '15:27', 80910, 6);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(6, '24:10', '26:17', 80910, 6);

INSERT INTO movies(id, title) VALUES(80894, '너의 결혼식');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(7, '07:40', '09:40', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(8, '09:55', '11:55', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(9, '12:15', '14:15', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(10, '14:35', '16:35', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(11, '16:55', '18:55', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(12, '19:15', '21:15', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(13, '21:35', '23:35', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(14, '24:00', '26:00', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(15, '26:15', '28:15', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(16, '28:30', '30:30', 80894, 5);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(17, '18:30', '20:30', 80894, 6);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(18, '10:50', '12:50', 80894, 3);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(19, '13:10', '15:10', 80894, 3);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(20, '15:30', '17:30', 80894, 3);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(21, '17:50', '19:50', 80894, 3);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(22, '20:10', '22:10', 80894, 3);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(23, '22:30', '24:30', 80894, 3);

INSERT INTO movies(id, title) VALUES(80596, '맘마미아!2');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(24, '07:10', '09:14', 80596, 4);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(25, '11:50', '13:54', 80596, 4);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(26, '21:25', '23:29', 80596, 4);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(27, '26:10', '28:14', 80596, 4);

INSERT INTO movies(id, title) VALUES(80861, '목격자');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(28, '09:30', '11:31', 80861, 4);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(29, '14:15', '16:16', 80861, 4);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(30, '19:00', '21:01', 80861, 4);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(31, '23:50', '25:51', 80861, 4);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(32, '28:30', '30:31', 80861, 4);

INSERT INTO movies(id, title) VALUES(80793, '미션 임파서블: 폴아웃');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(33, '28:00', '30:37', 80793, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(34, '10:25', '13:02', 80793, 6);

INSERT INTO movies(id, title) VALUES(80938, '상류사회');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(35, '06:30', '08:40', 80938, 2);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(36, '08:55', '11:05', 80938, 2);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(37, '11:20', '13:30', 80938, 2);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(38, '13:45', '15:55', 80938, 2);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(39, '16:15', '18:25', 80938, 2);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(40, '18:45', '20:55', 80938, 2);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(41, '21:15', '23:25', 80938, 2);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(42, '23:45', '25:55', 80938, 2);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(43, '26:25', '28:35', 80938, 2);

INSERT INTO movies(id, title) VALUES(80916, '서치');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(44, '07:30', '09:22', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(45, '09:40', '11:32', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(46, '12:00', '13:52', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(47, '14:10', '16:02', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(48, '16:25', '18:17', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(49, '18:35', '20:27', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(50, '20:45', '22:37', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(51, '22:55', '24:47', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(52, '25:05', '26:57', 80916, 1);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(53, '27:40', '29:32', 80916, 3);

INSERT INTO movies(id, title) VALUES(80810, '신과함께 인과 연');
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(54, '20:55', '23:26', 80810, 6);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(55, '08:00', '10:31', 80810, 3);
INSERT INTO plays(id, start, end, movie_id, screen_id) VALUES(56, '24:50', '27:21', 80810, 3);
